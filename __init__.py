import urllib.request
import json
import hashlib

from rdflib import Graph, Literal, URIRef, BNode, Namespace
from rdflib.namespace import FOAF, RDF, DC, DOAP

META = Namespace('http://pafcu.fi/ns/meta/')
def triple_uri(triple):
	return URIRef('triple:sha1:%s'%hashlib.sha1(bytes(str(triple),'utf-8')).hexdigest())

def get_profile(username):
	g = Graph()
	base_url = 'https://api.bitbucket.org/2.0'
	user_url = '%s/users/%s'%(base_url, username)

	user_uri = URIRef(user_url)

	with urllib.request.urlopen(user_url) as f:
		user_profile = json.loads(f.read().decode('utf-8'))

	if user_profile.get('website', None):
		g.add((user_uri, FOAF.homepage, URIRef(user_profile['website'])))

	repos_url = '%s/repositories/%s'%(base_url, username)
	with urllib.request.urlopen(repos_url) as f:
		repos = json.loads(f.read().decode('utf-8'))

	for repo in repos['values']:
		repourl = URIRef(repo['links']['self']['href'])
		g.add((repourl, RDF.type, DOAP.Project))
		if repo.get('name', None): g.add((repourl, DOAP.name, Literal(repo['name'])))
		if repo.get('website', None): g.add((repourl, DOAP.homepage, URIRef(repo['website'])))
		if repo.get('description', None): g.add((repourl, DOAP.description, Literal(repo['description'])))
		if repo.get('has_wiki', None): g.add((repourl, DOAP.wiki, URIRef(repo['links']['html']['href']+'/wiki')))
		if repo.get('has_issues', None): g.add((repourl, DOAP['bug-database'], URIRef(repo['links']['html']['href']+'/issues')))
		if repo.get('language', None): g.add((repourl, DOAP['programming-language'], Literal(repo['language'])))

		scm = repo['scm']
		repotypes = {'git':DOAP.GitRepository, 'hg': DOAP.HgRepository}
		repository = BNode()
		g.add((repository, RDF.type, repotypes[scm]))
		g.add((repository, DOAP.location, URIRef(repo['links']['clone'][0]['href']))) # TODO: Handle multiple locations
		g.add((repository, DOAP.browse, URIRef(repo['links']['html']['href'])))

		# TODO: Handle downloads

		g.add((repourl, DOAP.maintainer, user_uri))
		
	for triple in g:
		g.add((triple_uri(triple), META.sourceService, URIRef('https://bitbucket.org/')))
	# TODO: Add sourceDocument

	return g
